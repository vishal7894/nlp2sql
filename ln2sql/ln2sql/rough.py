# Import module
from tkinter import *
from pre_process import get_tables_info

# Create object
root = Tk()

# Adjust size
root.geometry("200x200")

# Dropdown menu options
options = [
    "all_star",
    "carrier_info",
    "student",
    "teaching",
    "professor",
]


# Change the label text
def show():
    tables_dict = get_tables_info("/Users/vishalch/Sigmoid/task_4_text_to_sql(Bhaskar)/final/ln2sql/ln2sql/database_store/school.sql")
    label.config(text= [column for column in [tables_dict[clicked.get()]]])


# datatype of menu text
clicked = StringVar()

# initial menu text
clicked.set(" ")

# Create Dropdown menu
drop = OptionMenu(root, clicked, *options)
drop.pack()

# Create button, it will change label text
button = Button(root, text="go", command=show).pack()

# Create Label
label = Label(root, text=" ")
label.pack()

# Execute tkinter
root.mainloop()

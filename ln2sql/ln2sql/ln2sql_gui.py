#!/usr/bin/python3
import tkinter.filedialog
from tkinter import *
from tkinter.messagebox import *

import os
# from .ln2sql import Ln2sql
from ln2sql import Ln2sql
from pre_process import add_space, add_table_name, get_operator, \
                        get_ordered_operator_list, get_tables_info, percent_conversion
from matcher import get_pattern, construct_sql_query
import pandas as pd

from constants import LN2SQL_DIR, DATABASE_DIR, LANGUAGE_DIR


class App:
    def __init__(self, root):
        root.title('ln2sql')
        root.bind('<Return>', self.parse)

        # Added table name block
        self.table_name_frame = LabelFrame(root, text="Input table name", padx=5, pady=5)
        self.table_name_frame.pack(fill="both", expand="yes", padx=10, pady=5)

        self.input_table_name_string = StringVar()
        self.input_table_name_string.set("carrier_info")
        self.input_table_name_entry = Entry(self.table_name_frame, textvariable=self.input_table_name_string, width=50)
        self.input_table_name_entry.pack()
        self.input_table_name_entry.bind('<Button-1>', self.clearEntry_2)

        self.sentence_frame = LabelFrame(root, text="Input Sentence", padx=5, pady=5)
        self.sentence_frame.pack(fill="both", expand="yes", padx=10, pady=5)

        self.input_sentence_string = StringVar()
        self.input_sentence_string.set("Enter a sentence...")
        self.input_sentence_entry = Entry(self.sentence_frame, textvariable=self.input_sentence_string, width=50)
        self.input_sentence_entry.pack()
        self.input_sentence_entry.bind('<Button-1>', self.clearEntry_1)

        # TODO columns block --> need a drop down
        # self.columns_frame = LabelFrame(root, text="Columns", padx=5, pady=5)
        # self.columns_frame.pack(fill="both", expand="yes", padx=10, pady=5)
        #
        # self.input_table_name_string = StringVar()
        # self.input_table_name_string.set("Enter a table name...")
        # self.input_table_name_entry = Entry(self.table_name_frame, textvariable=self.input_table_name_string, width=50)
        # self.input_table_name_entry.pack()
        # self.input_table_name_entry.bind('<Button-2>', self.clearEntry)

        # for database, thesaurus path

        # self.database_frame = LabelFrame(root, text="Database Selection", padx=5, pady=5)
        # self.database_frame.pack(fill="both", expand="yes", padx=10, pady=5)
        #
        # self.database_path_label = Label(self.database_frame, text="No SQL dump selected...")
        # self.database_path_label.pack(side="left")
        #
        # self.load_database_button = Button(self.database_frame, text="Choose a SQL dump", command=self.find_sql_file,
        #                                    width=20)
        # self.load_database_button.pack(side="right")
        #
        # self.language_frame = LabelFrame(root, text="Language Configuration Selection", padx=5, pady=5)
        # self.language_frame.pack(fill="both", expand="yes", padx=10, pady=5)
        #
        # self.language_path_label = Label(self.language_frame, text="No configuration file selected...")
        # self.language_path_label.pack(side="left")
        #
        # self.load_language_button = Button(self.language_frame, text="Choose a configuration file file",
        #                                    command=self.find_csv_file, width=20)
        # self.load_language_button.pack(side="right")
        #
        # self.thesaurus_frame = LabelFrame(root, text="Import your personal thesaurus ?", padx=5, pady=5)
        # self.thesaurus_frame.pack(fill="both", expand="yes", padx=10, pady=5)
        #
        # self.thesaurus_path_label = Label(self.thesaurus_frame, text="No thesaurus selected...")
        # self.thesaurus_path_label.pack(side="left")
        #
        # self.load_thesaurus_button = Button(self.thesaurus_frame, text="Choose a thesaurus file",
        #                                     command=self.find_thesaurus_file, width=20)
        # self.load_thesaurus_button.pack(side="right")


        self.go_button = Button(root, text="Go!", command=self.lanch_parsing)
        self.go_button.pack(side="right", fill="both", expand="yes", padx=10, pady=2)

        self.reset_button = Button(root, text="Reset", fg="red", command=self.reset_window)
        # self.reset_button = Button(root, text="get columns", fg="red", command=self.get_column_names)
        self.reset_button.pack(side="right", fill="both", expand="yes", padx=10, pady=2)

    def clearEntry_1(self, event):
        self.input_sentence_string.set("")

    def clearEntry_2(self, event):
        self.input_table_name_string.set("")

    def parse(self, event):
        self.lanch_parsing()

    # def find_sql_file(self):
    #     filename = tkinter.filedialog.askopenfilename(title="Select a SQL file",
    #                                                   filetypes=[('sql files', '.sql'), ('all files', '.*')])
    #     self.database_path_label["text"] = filename
    #
    # def find_thesaurus_file(self):
    #     filename = tkinter.filedialog.askopenfilename(title="Select a thesaurus file",
    #                                                   filetypes=[('thesaurus files', '.dat'), ('all files', '.*')])
    #     self.thesaurus_path_label["text"] = filename
    #
    # def find_csv_file(self):
    #     filename = tkinter.filedialog.askopenfilename(title="Select a language configuration file",
    #                                                   filetypes=[('csv files', '.csv'), ('all files', '.*')])
    #     self.language_path_label["text"] = filename

    def reset_window(self):
        # self.database_path_label["text"] = "No SQL dump selected..."
        # self.thesaurus_path_label["text"] = "No thesaurus selected..."
        self.input_sentence_string.set("Enter a sentence...")
        # self.language_path_label["text"] = "No configuration file selected..."
        self.input_table_name_string.set("Enter a table name")
        return

    # TODO get columns function
    # def get_column_names(self):
    #     table_dict = get_tables_info(os.path.join(DATABASE_DIR,"school.sql")
    #     columns = table_dict[self.input_table_name_string.get()]
    #     return

    def lanch_parsing(self):
        try:
            # thesaurus_path = None
            # str(self.input_table_name_string.get()) = 'carrier_info'
            # if str(self.thesaurus_path_label["text"]) != "No thesaurus selected...":
            #     thesaurus_path = str(self.thesaurus_path_label["text"])

            # if (str(self.database_path_label["text"]) != "No SQL dump selected...") and (
            #             str(self.language_path_label["text"]) != "No configuration file selected...") and (
            #             str(self.input_sentence_string.get()) != "Enter a sentence..."):

            if (str(self.input_sentence_string.get()) != "Enter a sentence...") and \
                    (str(self.input_table_name_string.get()) != "Enter a table name..."):
                # query = Ln2sql(self.database_path_label["text"],
                #        self.language_path_label["text"], thesaurus_path=thesaurus_path,
                #        json_output_path='./output.json').get_query(self.input_sentence_string.get(), col_flag=False)
                #
                # query = Ln2sql(
                #     "/Users/vishalch/Sigmoid/task_4_text_to_sql(Bhaskar)/text2sql/ln2sql/ln2sql/database_store/insurance.sql",
                #     "/Users/vishalch/Sigmoid/task_4_text_to_sql(Bhaskar)/text2sql/ln2sql/ln2sql/lang_store/english.csv",
                #     thesaurus_path=thesaurus_path,
                #        json_output_path='./output.json').get_query(self.input_sentence_string.get(), col_flag=False)

                mod_sentence = add_space(self.input_sentence_string.get(), LN2SQL_DIR)

                modified_sentence = get_operator(mod_sentence, LN2SQL_DIR, 'operators.csv')
                modified_sentence = get_operator(modified_sentence, LN2SQL_DIR, 'math_operators.csv')

                operators_list, math_operator_list = get_ordered_operator_list(modified_sentence, LN2SQL_DIR)

                # Re check Percentage
                modified_sentence, percent_convert_flag = percent_conversion(modified_sentence)

                if percent_convert_flag:
                    utterance = add_table_name(modified_sentence, self.input_table_name_string.get())

                else:
                    utterance = add_table_name(mod_sentence, self.input_table_name_string.get())

                queries, result = Ln2sql(
                    os.path.join(DATABASE_DIR, "school.sql"),
                    os.path.join(LANGUAGE_DIR, "english.csv"),
                    # thesaurus_path=thesaurus_path,
                    json_output_path='./output.json').get_query(utterance, col_flag=False)

                if result['agg_functions']:
                    showinfo('Result : {}'.format(queries), queries)

                elif not result['agg_functions']:
                    result['operators'] = operators_list
                    result['math_operators'] = math_operator_list
                    result['table_name'] = self.input_table_name_string.get()

                    print("*"*50)
                    print("\n")
                    print(len(result['columns_of_select']),
                          len(result['columns_of_where']),
                          len(result['columns_of_values_of_where']),
                          len(result['operators']),
                          len(result['math_operators']))
                    print("\n")
                    print(result)
                    print("\n")
                    print(utterance)

                    pattern_file = "pattern.csv"

                    patterns = pd.read_csv(os.path.join(LN2SQL_DIR, pattern_file))

                    template = get_pattern(result['columns_of_select'],
                                           result['columns_of_where'],
                                           result['columns_of_values_of_where'],
                                           result['operators'],
                                           result['math_operators'],
                                           patterns
                                           )

                    query = construct_sql_query(template,
                                                result['columns_of_select'],
                                                result['columns_of_where'],
                                                result['columns_of_values_of_where'],
                                                result['operators'],
                                                result['math_operators'],
                                                result['table_name']
                                                )

                    print(self.input_sentence_string.get())
                    print(query)

                    showinfo('Result : {}'.format(query), query)
            else:
                showwarning('Warning', 'You must fill in all fields, please.')
        except Exception as e:
            showwarning('Error', e)
        return


root = Tk()
App(root)
root.resizable(width=FALSE, height=FALSE)
root.mainloop()

import os
from database import Database
from fuzzywuzzy import fuzz
from constants import PROJECT_DIR, LN2SQL_DIR


def percent_conversion(modified_sentence):
    modified_sentence_tokens = modified_sentence.split(" ")

    if "PERCENTAGE" in modified_sentence:
        percentage_index = modified_sentence_tokens.index("PERCENTAGE")
        value_index = percentage_index - 1
        value = modified_sentence_tokens[value_index]
        if "." in value:
            x = value.split(".")
            score = 0
            for i in x:
                if i.isnumeric():
                    score += 1
            if score == 2:
                value = float(value)
        elif value.isnumeric():
            value = int(value)

        percent_conversion = value * 0.01

        modified_sentence_tokens[value_index] = percent_conversion
        modified_sentence_tokens.pop(percentage_index)

        modified_sentence = " ".join([str(elem) for elem in modified_sentence_tokens])

        return modified_sentence, True
    else:
        pass

    return modified_sentence, False


def get_tables_info(database_path):
    tables_info = dict()

    database = Database()
    database.load(database_path)
    for table in database.tables:
        tables_info[table.name] = []
        for col in table.get_columns():
            tables_info[table.name].append(col.name)

    return tables_info


def get_operator(utterance, file_path, file_name):
    with open(os.path.join(file_path, file_name), 'r') as file:
        content = file.readlines()

    # import pdb;pdb.set_trace()
    operators_list = []
    utterance_tokens = utterance.split(" ")
    for n_line, line in enumerate(content):
        map_words = line.split(",")
        map_words = [word.strip(" ") for word in map_words]
        operator, map_words = map_words[0], map_words[1:]
        # operators_list.append(operator)
        for idx, map_word in enumerate(map_words):
            len_map_word = len(map_word.split(" "))
            map_word = map_word.rstrip("\n")
            # if len_map_word == 1:
            if (map_word in utterance) & (map_word in utterance_tokens):
                start_pos = utterance.index(map_word)
                utterance = utterance.replace(utterance[start_pos:start_pos+len(map_word)], operator)
                index = utterance_tokens.index(map_word)
                if index+1 <= len(utterance_tokens):
                    next_word = utterance_tokens[index+1]
                    concat_word = " ".join([str(elem) for elem in utterance_tokens[index:index+2]])
                    if concat_word in map_words:
                        utterance = utterance.replace(next_word, "")
                        if index+2 <= len(utterance_tokens):
                            next_word_2 = utterance_tokens[index+2]
                            concat_word_2 = " ".join([str(elem) for elem in utterance_tokens[index:index+3]])
                            if concat_word_2 in map_words:
                                utterance = utterance.replace(next_word_2, "")

    return utterance


# operators_file = 'operators.csv'
# operators_path = os.path.join(LN2SQL_DIR, operators_file)
# # sentence = r"Show all student where age greater than 10"
# # sentence = r"Show all rows where year less than 1950 and league_id equal to 1"
# # sentence = r"Select sum of Amount_Claimed where Claim_ID is greater than 5 from Claims"
# # sentence = r"select rows where Amount_Claimed is greater than Claim_ID plus Policy_ID"
# # sentence = r"select rows where Amount_Claimed is greater than ten percent of Claim_ID minus Policy_ID"
# # sentence = r"select rows where Amount_Claimed is greater than 10 percent of Claim_ID minus Policy_ID"
# # sentence = r"Select all rows where Col1 greater than 10 % of ( Col2 by Col3 ) and col4 greater than ( col2 plus Col3 )"
# # sentence = r"Select all Claims where Amount_Claimed is within 10 and 15"
# # sentence = r"Select all Claims where ( Claim_ID plus Policy_ID ) greater than 5 times Amount_Claimed"
# sentence = r"Select all Claims where ( Claim_ID plus Policy_ID ) greater than 5 times Amount_Claimed"
#
# 1,1,1,1,"SELECT SUM ( columns_of_select[0] ) from table WHERE columns_of_where[0] operators[0] columns_of_values_of_where[0]"
# 0,3,1,1,2,"SELECT * FROM table WHERE columns_of_where[0] operators[0] columns_of_values_of_where[0] math_operators[0] ( columns_of_where[1] math_operator[1] columns_of_where[2] )"

# print(sentence)
# get_operator(sentence, operators_path)

def add_space(sentence, char_path):
    with open(os.path.join(char_path, 'characters.csv'), 'r') as file:
        content = file.readlines()

    char_list = []
    for idx, line in enumerate(content):
        char_list.append(line.rstrip("\n"))

    for count, char in enumerate(char_list):
        if char in sentence:
            sentence = sentence.replace(char, " {} ".format(char))

    return sentence

# char_path = r"/Users/vishalch/Sigmoid/task_4_text_to_sql(Bhaskar)/text2sql/ln2sql/ln2sql/"

# print(add_space("Select all Claims where (Claim_ID+Policy_ID) greater than 5*Amount_Claimed", char_path))


def get_ordered_operator_list(sentence, path):
    with open(os.path.join(path, 'operators.csv'), 'r') as file:
        content = file.readlines()

    operator_list = []
    ordered_operator_list = []
    for n_line, line in enumerate(content):
        line = line.split(",")
        operator, map_words = line[0], line[1:]
        map_words = [word.strip(" ") for word in map_words]
        operator_list.append(operator)
    sentence_tokens = sentence.split(" ")

    for token in sentence_tokens:
        if token in operator_list:
            ordered_operator_list.append(token)

    with open(os.path.join(path, 'math_operators.csv'), 'r') as file:
        content = file.readlines()

    math_operator_list = []

    ordered_math_operator_list = []
    for n_line, line in enumerate(content):
        line = line.split(",")
        operator, map_words = line[0], line[1:]
        map_words = [word.strip(" ") for word in map_words]
        math_operator_list.append(operator)

    sentence_tokens = sentence.split(" ")
    for token in sentence_tokens:
        if token in math_operator_list:
            ordered_math_operator_list.append(token)

    if 'PERCENTAGE' in ordered_math_operator_list:
        ordered_math_operator_list.pop(ordered_math_operator_list.index('PERCENTAGE'))

    return ordered_operator_list, ordered_math_operator_list


def add_table_name(utterance, table_name):
    utterance_token = utterance.split(" ")
    if 'where' in utterance_token:
        index_of_where = utterance_token.index("where")
        to_insert = 'from {}'.format(table_name)
        utterance_token.insert(index_of_where, to_insert)
        new_utterance = ' '.join([str(elem) for elem in utterance_token])

    else:
        # TODO for aggregates and group by --> check if this works or not
        new_utterance = utterance + " from {}".format(table_name)

    return new_utterance


def tweak_utterance(utterance, table_name, tables_info, threshold=0.8):
    tokens = utterance.split(" ")
    columns = tables_info[table_name]

    ratios = dict()

    # for token in tokens:
    #     ratios[token] = process.extract(token, columns)

    count = 0
    for col in columns:
        ratios[col] = []
        for token in tokens:
            rat = fuzz.ratio(col, token)
            if rat >= threshold*100:
                count += 1
                ratios[col].append([token, rat])

    keys = ratios.keys()
    new_ratios = dict()

    for key in keys:
        if len(ratios[key]) > 0:
            new_ratios[key] = ratios[key]

    x = utterance
    for item in new_ratios.items():
        col = item[0]
        token = item[1][0][0]
        # print("col --> {}, token-->{}".format(col, token))
        x = x.replace(token, col)
        # print(x)

    new_utterance = table_name + " " + x
    return new_utterance


# path = r"/Users/vishalch/Sigmoid/task_4_text_to_sql(Bhaskar)/nl2sql/ln2sql/ln2sql/ln2sql/database_store/school.sql"
# print(get_tables_info(path))
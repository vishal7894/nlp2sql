import streamlit as st

import os
from ln2sql import Ln2sql
from pre_process import add_space, add_table_name, get_operator, \
                        get_ordered_operator_list, get_tables_info, percent_conversion
from matcher import get_pattern, construct_sql_query
import pandas as pd

from constants import LN2SQL_DIR, DATABASE_DIR, LANGUAGE_DIR

# st.title("Text to SQL query")
st.markdown("<h1 style='text-align: center; color: White;'>Text to SQL query</h1>", unsafe_allow_html=True)

tables = get_tables_info(os.path.join(DATABASE_DIR, 'school.sql')).keys()
table_name = st.selectbox("Tables in database", tables)

# columns = []

if table_name:
    # columns.extend(get_tables_info(os.path.join(DATABASE_DIR, 'school.sql'))[table_name])
    columns = get_tables_info(os.path.join(DATABASE_DIR, 'school.sql'))[table_name]
    columns_df = pd.DataFrame(columns)
    columns_df.columns = ['columns']

    if st.checkbox("Get columns"):
        st.dataframe(columns_df)
        # col = st.radio("columns", columns)
        # st.write("Table {} columns".format(table_name))
        # for i in range(len(columns)):
        #     st.write(i+1, " ", columns[i])

sentence = st.text_input("Enter a sentence")

# if col:
#     sentence

if (table_name != '') & (sentence != '') & st.button('Go'):

    mod_sentence = add_space(sentence, LN2SQL_DIR)
    # import pdb;pdb.set_trace()
    modified_sentence = get_operator(mod_sentence, LN2SQL_DIR, 'operators.csv')

    modified_sentence = get_operator(modified_sentence, LN2SQL_DIR, 'math_operators.csv')

    operators_list, math_operator_list = get_ordered_operator_list(modified_sentence, LN2SQL_DIR)

    # Re check Percentage
    modified_sentence, percent_convert_flag = percent_conversion(modified_sentence)

    if percent_convert_flag:
        utterance = add_table_name(modified_sentence, table_name)

    else:
        utterance = add_table_name(mod_sentence, table_name)

    queries, result = Ln2sql(
        os.path.join(DATABASE_DIR, "school.sql"),
        os.path.join(LANGUAGE_DIR, "english.csv"),
        # thesaurus_path=thesaurus_path,
        json_output_path='./output.json').get_query(utterance, col_flag=False)

    if result['agg_functions']:
        st.markdown("<h3 style='text-align: center; color: White;'>Text Input</h3>", unsafe_allow_html=True)
        st.markdown("<h4 style='text-align: center; color: White;'>{}</h4>".format(sentence), unsafe_allow_html=True)
        st.markdown("<h3 style='text-align: center; color: White;'>SQL query</h3>", unsafe_allow_html=True)
        st.markdown("<h4 style='text-align: center; color: White;'>{}</h4>".format(queries), unsafe_allow_html=True)
        # st.write("SQL query \n")
        # st.write(queries)
        # showinfo('Result : {}'.format(queries), queries)

    elif not result['agg_functions']:
        result['operators'] = operators_list
        result['math_operators'] = math_operator_list
        result['table_name'] = table_name

        print("*" * 50)
        print("\n")
        print(len(result['columns_of_select']),
              len(result['columns_of_where']),
              len(result['columns_of_values_of_where']),
              len(result['operators']),
              len(result['math_operators']))
        print("\n")
        print(result)
        print("\n")
        print(utterance)

        pattern_file = "pattern.csv"

        patterns = pd.read_csv(os.path.join(LN2SQL_DIR, pattern_file))

        template = get_pattern(result['columns_of_select'],
                               result['columns_of_where'],
                               result['columns_of_values_of_where'],
                               result['operators'],
                               result['math_operators'],
                               patterns
                               )

        query = construct_sql_query(template,
                                    result['columns_of_select'],
                                    result['columns_of_where'],
                                    result['columns_of_values_of_where'],
                                    result['operators'],
                                    result['math_operators'],
                                    result['table_name']
                                    )

        print(sentence)
        print(query)
        st.markdown("<h3 style='text-align: center; color: White;'>Text Input</h3>", unsafe_allow_html=True)
        st.markdown("<h4 style='text-align: center; color: White;'>{}</h4>".format(sentence), unsafe_allow_html=True)
        st.markdown("<h3 style='text-align: center; color: White;'>SQL query</h3>", unsafe_allow_html=True)
        st.markdown("<h4 style='text-align: center; color: White;'>{}</h4>".format(query), unsafe_allow_html=True)

import argparse

# from .ln2sql import Ln2sql
import os
from ln2sql import Ln2sql
from pre_process import add_space, add_table_name, get_operator, get_ordered_operator_list
from matcher import get_pattern, construct_sql_query
import pandas as pd

from constants import LN2SQL_DIR


def main():
    arg_parser = argparse.ArgumentParser(description='A Utility to convert Natural Language to SQL query')
    arg_parser.add_argument('-d', '--database', help='Path to SQL dump file', required=True)
    arg_parser.add_argument('-l', '--language', help='Path to language configuration file', required=True,
                            default="/Users/vishalch/Sigmoid/task_4_text_to_sql(Bhaskar)/text2sql/ln2sql/ln2sql/lang_store/english.csv")
    arg_parser.add_argument('-i', '--sentence', help='Input sentence to parse', required=True)
    arg_parser.add_argument('-j', '--json_output', help='path to JSON output file', default="output.json")
    arg_parser.add_argument('-t', '--thesaurus', help='path to thesaurus file', default=None)
    arg_parser.add_argument('-s', '--stopwords', help='path to stopwords file', default=None)
    arg_parser.add_argument('-tb', '--table_name', help='name of table being used', default=None)

    args = arg_parser.parse_args()

    mod_sentence = add_space(args.sentence, LN2SQL_DIR)

    modified_sentence = get_operator(mod_sentence, LN2SQL_DIR, 'operators.csv')
    modified_sentence = get_operator(modified_sentence, LN2SQL_DIR, 'math_operators.csv')

    operators_list, math_operator_list = get_ordered_operator_list(modified_sentence, LN2SQL_DIR)

    utterance = add_table_name(args.sentence, args.table_name)

######################        OLD BLOCK        ######################
    # utterance = tweak_utterance(args.sentence, args.table_name, tables_info)

    # ln2sql, columns_of_values_of_where, columns_of_where, columns_of_select, table_name = Ln2sql(
    #     database_path=args.database,
    #     language_path=args.language,
    #     json_output_path=args.json_output,
    #     thesaurus_path=args.thesaurus,
    #     stopwords_path=args.stopwords,
    # ).get_query(utterance, col_flag=False)
######################        OLD BLOCK        ######################

    # # TODO write function for keywords working fine without templates
    # keywords = ['between', 'sum']
    # for keyword in keywords:
    #     if keyword in args.sentence:
    #         col_flag = False
    #         break
    #     else:
    #         col_flag = True
    #         break

    result = Ln2sql(
        database_path=args.database,
        language_path=args.language,
        json_output_path=args.json_output,
        thesaurus_path=args.thesaurus,
        stopwords_path=args.stopwords,
    ).get_query(utterance, col_flag=False)




    # print(ln2sql)
    # # if col_flag:
    # result['operators'] = operators_list
    # result['math operators'] = math_operator_list
    #
    # print(result)
    #
    # pattern_file = "pattern.csv"
    #
    # patterns = pd.read_csv(os.path.join(LN2SQL_DIR, pattern_file))
    #
    # print(patterns)
    # template = get_pattern(columns_of_select, columns_of_where, columns_of_values_of_where,
    #                        operators_list, math_operator_list, patterns)
    #
    # query = construct_sql_query(template, columns_of_select, columns_of_where, columns_of_values_of_where,
    #                             operators_list, math_operator_list, table_name)
    # print(args.sentence)
    # print(query)


if __name__ == '__main__':
    main()


# python3 ln2sql/main.py -i "Select all Claims where Claim_ID greater than 5" -l "/Users/vishalch/Sigmoid/task_4_text_to_sql(Bhaskar)/text2sql/ln2sql/ln2sql/lang_store/english.csv" -d "/Users/vishalch/Sigmoid/task_4_text_to_sql(Bhaskar)/text2sql/ln2sql/ln2sql/database_store/insurance.sql"

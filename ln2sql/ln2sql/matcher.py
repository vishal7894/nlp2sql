import pandas as pd

# ######   pattern 1  ########

# columns_of_select = []
# columns_of_where = ['gp']
# columns_of_values_of_where = [10]
# operators = ['<']   # need to be extracted from input sentence

# ######   pattern 2  ########

# columns_of_select = []
# columns_of_where = ['gp', 'starting_pos']
# columns_of_values_of_where = [10, 11]
# operators = ['<','=']   # need to be extracted from input sentence

# ######   pattern 3   ########
#
# columns_of_select = []
# columns_of_where = ['starting_pos', 'gp']
# columns_of_values_of_where = []
# operators = ['<']   # need to be extracted from input sentence

# #####   pattern 4  ########
#
# columns_of_select = []
# columns_of_where = ['starting_pos', 'gp', 'year']
# columns_of_values_of_where = []
# operators = ['<', '+']  # need to be extracted from input sentence

######   pattern 5  ########

# columns_of_select = []
# columns_of_where = ['starting_pos', 'gp', 'year']
# columns_of_values_of_where = []
# operators = []  # need to be extracted from input sentence

######   pattern 6  ########

# columns_of_select = []
# columns_of_where = ['Amount_Claimed', 'Claim_ID', 'Policy_ID']
# columns_of_values_of_where = [0.1]
# operators = ['*', '-']  # need to be extracted from input sentence

######   pattern 7  ########
#
# columns_of_select = []
# columns_of_where = ['starting_pos']
# columns_of_values_of_where = [10, 30]
# operators = []  # need to be extracted from input sentence

# ######   pattern 8  ########
#
# columns_of_select = []
# columns_of_where = ['starting_pos', 'gp', 'year']
# columns_of_values_of_where = [10]
# operators = ['+', ">", "*"]  # need to be extracted from input sentence


# already known
# table_name = "all_star"
# table_name = "Claims"

# pattern_path = r"/Users/vishalch/Sigmoid/task_4_text_to_sql(Bhaskar)/nl2sql/ln2sql/ln2sql/ln2sql/pattern.csv"
#IMPORTANT --> In every pattern, wherever any kind of values need to be imputed, there should not be any spaces
# patterns = pd.read_csv(pattern_path)

# TODO further pre process of input sentence --> refer to queries.txt line 127 (NEW APPROACH)


def get_pattern(columns_of_select, columns_of_where,
                columns_of_values_of_where, operators,
                math_operators, patterns):

    # import pdb;pdb.set_trace()
    n_cols_of_select = len(columns_of_select)
    n_cols_of_where = len(columns_of_where)
    n_cols_of_values_of_where = len(columns_of_values_of_where)
    n_operators = len(operators)
    n_math_operators = len(math_operators)

    df = patterns.copy()
    # print(df.columns)
    loc = df.loc[(df['columns_of_select'] == n_cols_of_select) & (df['columns_of_where'] == n_cols_of_where) &
            (df['columns_of_values_of_where'] == n_cols_of_values_of_where) & (df['operators'] == n_operators) &
                 (df['math_operators'] == n_math_operators)]
    req_patterns = loc['pattern'].to_list()

    # TODO what if there are multiple patterns having same number of n_cols_of_select, n_operators, ...
    # TODO potential solution --> branching based on keywords
    # try:
    if len(req_patterns) == 1:
        template = req_patterns[0]
    return template


def construct_sql_query(template, columns_of_select, columns_of_where,
                        columns_of_values_of_where,
                        operators, math_operators, table_name):
    template_tokens = template.split(" ")

    index = template_tokens.index('table')
    template_tokens[index] = table_name

    # TODO make one for loop for all lists (redundant code)
    for idx, col in enumerate(columns_of_select):
        index = template_tokens.index('columns_of_select[{}]'.format(idx))
        template_tokens[index] = columns_of_select[idx]

    for idx, col in enumerate(columns_of_where):
        index = template_tokens.index('columns_of_where[{}]'.format(idx))
        template_tokens[index] = columns_of_where[idx]

    for idx, col in enumerate(columns_of_values_of_where):
        index = template_tokens.index('columns_of_values_of_where[{}]'.format(idx))
        template_tokens[index] = columns_of_values_of_where[idx]

    for idx, col in enumerate(operators):
        index = template_tokens.index('operators[{}]'.format(idx))
        template_tokens[index] = operators[idx]

    for idx, col in enumerate(math_operators):
        index = template_tokens.index('math_operators[{}]'.format(idx))
        template_tokens[index] = math_operators[idx]

    query = ' '.join([str(elem) for elem in template_tokens])
    return query


# template = get_pattern(columns_of_select, columns_of_where, columns_of_values_of_where, operators, patterns)
# query = construct_sql_query(template, columns_of_select, columns_of_where, columns_of_values_of_where, operators, table_name)
# print(query)
